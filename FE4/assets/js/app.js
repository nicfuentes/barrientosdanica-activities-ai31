var ctx1 = document.getElementById('barchart');
var barchart = new Chart(ctx1, {
    type: 'bar',
    data: {
        labels: ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUNE', 'JULY'],
        datasets: [{
            label: 'Borrowed',
            data: [12, 19, 3, 5, 2, 3, 4],
            backgroundColor: [
                'rgba(124, 87, 157, 1)',
                'rgba(124, 87, 157, 1)',
                'rgba(124, 87, 157, 1)',
                'rgba(124, 87, 157, 1)',
                'rgba(124, 87, 157, 1)',
                'rgba(124, 87, 157, 1)',
                'rgba(124, 87, 157, 1)',
            ],
            borderWidth: 1
        },
        {
            label: 'Returned',
            data: [12, 19, 3, 5, 2, 3, 10],
            backgroundColor: [
                'rgba(209, 106, 151, 1)',
                'rgba(209, 106, 151, 1)',
                'rgba(209, 106, 151, 1)',
                'rgba(209, 106, 151, 1)',
                'rgba(209, 106, 151, 1)',
                'rgba(209, 106, 151, 1)',
                'rgba(209, 106, 151, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});

var ctx2 = document.getElementById('piechart');
var piechart = new Chart(ctx2, {
    type: 'doughnut',
    data: {
        labels: ['Romance Novel', 'Narrative', 'History', 'Thriller', 'Horror Fiction'],
        datasets: [{
            label: '# of Votes',
            data: [12, 3, 13, 5, 2],
            backgroundColor: [
                'rgba(124, 87, 157, 1)',
                'rgba(253, 158, 130, 1)',
                'rgba(172, 93, 158, 1)',
                'rgba(209, 106, 151, 1)',
                'rgba(238, 128, 141, 1)',
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});