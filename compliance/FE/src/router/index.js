import Vue from 'vue'
import VueRouter from 'vue-router'
import Music from '../views/Music.vue'
import Login from '../views/Admin/Login.vue'
import Home from '../views/Admin/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/music',
    name: 'Music',
    component: Music
  },
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/',
    name: 'Home',
    component: Home
  },

]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
