import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import VueFileAgent from 'vue-file-agent';
import VueAPlayer from 'vue-aplayer'
import VueFileAgentStyles from 'vue-file-agent/dist/vue-file-agent.css';
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import Toast from "vue-toastification";
// Import the CSS or use your own!
import "vue-toastification/dist/index.css";
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import './assets/css/main.css'

const options = {
  // You can set your default options here
  transition: "Vue-Toastification__bounce",
  maxToasts: 5,
  pauseOnHover: false,
  hideProgressBar: true,
};

Vue.use(Toast, options);
Vue.use(VueFileAgent);
Vue.use(BootstrapVue);
Vue.use(IconsPlugin);
Vue.component('aplayer', VueAPlayer);
VueAPlayer.disableVersionBadge = true
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
