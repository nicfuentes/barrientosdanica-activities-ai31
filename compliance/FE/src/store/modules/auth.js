import axios from '@/axios'
const AUTH = "auth";
export default {
    state: {
        token: localStorage.getItem("auth") || ""
    },
    getters: {
        GET_USER(state) {
            return state.user;
        },
        GET_TOKEN(state) {
            return state.token;
        },
    },
    mutations: {
        SET_USER(state, user) {
            state.user = user;
        },
        SET_TOKEN(state, token) {
            localStorage.setItem("auth", token);
            state.token = token;

            const BEARER_TOKEN = localStorage.getItem("auth") || "";
            axios.defaults.headers.common["Authorization"] = `Bearer ${BEARER_TOKEN}`;
        },
        UNSET_USER(state) {
            localStorage.removeItem("auth");
            state.token = "";

            axios.defaults.headers.common["Authorization"] = "";
        },
    },
    actions: {
        async LOGIN({ commit }, user) {
            const res = await axios.post(`${AUTH}/login`, user)
                .then((response) => {
                    commit("SET_USER", response.data.user);
                    commit("SET_TOKEN", response.data.access_token);

                    return response;
                })
                .catch((error) => {
                    return error.response;
                });

            return res;
        },
        async CHECK_USER({ commit }) {
            const res = await axios.get(
                `${AUTH}/me?token=` + localStorage.getItem("auth")
            )
                .then((response) => {
                    commit("SET_USER", response.data);
                    commit("SET_TOKEN", localStorage.getItem("auth"));
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });

            return res;
        },
        async LOGOUT({ commit }) {
            const res = await axios.post(`${AUTH}/logout?`)
                .then((response) => {
                    commit("UNSET_USER");
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });

            return res;
        },
    }
}