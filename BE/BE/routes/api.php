<?php

use App\Http\Controllers\BookController;
use App\Http\Controllers\PatronController;
use App\Http\Controllers\BorrowedBookController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ReturnedBookController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


Route::apiResource('patrons', PatronController::class);
Route::apiResource('books', BookController::class);
Route::apiResource('/borrowedbook', BorrowedBookController::class);
Route::apiResource('/returnedbook', ReturnedBookController::class);
Route::get('/categories', [CategoryController::class, 'index']);
